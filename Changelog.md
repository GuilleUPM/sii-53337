# Changelog


Aquí se describen los cambios realizados en el repositorio

## [v1.4] - 16-12-2020
-Se añade el programa cliente y servidor
-Se implemeta tubería de comunicación entre el servidor y el cliente para compartir imformación del juego.
-Se elimina el programa tenis y mundo.
-Se implementa hilo y tubería para compartir la pulsación de cada tecla.

## [v1.3] - 2-12-2020
-Se crea logger que informará del tanteo de la partida
-Se programa el bot el cual jugará automáticamente por el jugador 1
-Se implementa la finalización de la partida cuando se llega a los 3 puntos por parte de uno de los jugadores

## [v1.2] - 18-11-2020
-Se implementa la velocidad en la esfera y en las raquetas
-Se modifica el juego para implementar una reducción del radio de la esfera de forma gradual en cada punto

## [v1.1] - 29-10-2020
-Modifico las cabeceras de los códigos fuente
-Añado etiqueta
-Changelog añadido al proyecto



