#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "Marcador.h"

int main()
{
	marcador tanteo;
	int fifo_fd;

	if(mkfifo("/tmp/logger_fifo",0777) < 0)
	{
		perror("FIFO mal creada");
		return 1;
	}

	if((fifo_fd = open("/tmp/logger_fifo", O_RDONLY))<0)
	{
		perror("Error al abrir FIFO");
		return 1;
	}
	while(1)
	{
	int lectura=read(fifo_fd, &tanteo, sizeof(tanteo));
	if(lectura>=sizeof(tanteo))
	{
		if(tanteo.ult_p == 1) {
			printf("Jugador 1 marca 1 punto, lleva %d puntos\n", tanteo.j_1);
		}
		if(tanteo.ult_p == 2){
			printf("Jugador 2 marca 1 punto, lleva %d puntos\n", tanteo.j_2);
		}
	}
	else if(lectura == 0)
	{
		printf("Fin del juego\n");
		return 0;
	}
}

close(fifo_fd);
unlink("/tmp/logger_fifo");
printf("terminado");
return 0;

}

